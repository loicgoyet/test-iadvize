import gulp from 'gulp'
import browserify from 'browserify'
import babelify from 'babelify'
import source from 'vinyl-source-stream'
import buffer from 'vinyl-buffer'
import eslint from 'gulp-eslint'

import gulpLoadPlugins from 'gulp-load-plugins';
const $ = gulpLoadPlugins();


import browserSync from 'browser-sync'

const reload = browserSync.reload

gulp.task('lint-script', () =>
  gulp.src('src/scripts/**/*.js')
    .pipe(eslint())
    .pipe(eslint.format())
)

gulp.task('build-script', ['lint-script'], () =>
  browserify({ entries: './src/scripts/index.js', debug: true })
    .transform('babelify', { presets: ['es2015', 'react', 'stage-2'] })
    .bundle()
    .pipe(source('index.js'))
    .pipe(buffer())
    .pipe($.sourcemaps.init())
    .pipe($.sourcemaps.write('./maps'))
    .pipe(gulp.dest('./dist/js'))
);

gulp.task('build-template', () =>
  gulp.src('src/index.html')
    .pipe(gulp.dest('./dist'))
);

const reportError = function(err) {
  $.notify({
    title: 'An error occured with a gulp task',
  }).write(err);

  console.log(err.toString());
  this.emit('end');
}

gulp.task('build-style', () =>
  gulp.src('src/style/main.scss')
    .pipe($.sourcemaps.init())
    .pipe($.sassGlob())
    .pipe($.sass({
      includePaths: ['src/style/utils'],
    })).on('error', reportError)
    .pipe($.autoprefixer())
    .pipe($.sourcemaps.write())
    .pipe($.csso())
    .pipe($.rename({ suffix: '.min' }))
    .pipe(gulp.dest('dist/style'))
);

gulp.task('build', ['build-template', 'build-script', 'build-style'])

gulp.task('serve', ['build'], () => {
  browserSync({
    notify: false,
    // Customize the Browsersync console logging prefix
    logPrefix: 'iAdvize LG',
    server: ['.tmp', 'dist'],
    port: 3000
  });

  gulp.watch(['src/**/*.js'], ['build-script', reload]);
  gulp.watch(['src/**/*.scss'], ['build-style', reload]);
  gulp.watch(['src/index.html'], ['build-template', reload]);
});


gulp.task('default', ['serve']);
