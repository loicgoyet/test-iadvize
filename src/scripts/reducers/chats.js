import {
  ADD_MESSAGE,
  ADD_CHAT,
  UPDATE_PROFILE_STATUS
} from '../actions/chats'

const chats = (state = [], action) => {
  switch (action.type) {
    case ADD_CHAT: {
      return [
        ...state,
        {
          id: action.chatId,
          log: [],
          status: [],
        }
      ]
    }

    case ADD_MESSAGE: {
      return state.map(chat =>
        (chat.id === action.chatId)
          ? {
            ...chat,
            log: [
              ...chat.log,
              {
                id: action.messageId,
                author: action.author,
                createdAt: action.createdAt,
                message: action.message,
              }
            ],
          }
          : chat
      )
    }

    case UPDATE_PROFILE_STATUS: {
      return state.map(chat =>
        (chat.id === action.chatId)
          ? {
            ...chat,
            status: {
              ...chat.status,
              [action.profile]: action.status,
            },
          }
          : chat
      )
    }

    default: {
      return state
    }
  }
}

export default chats
