export const ADD_CHAT = 'ADD_CHAT'
export const addChat = id => {
  return {
    type: ADD_CHAT,
    chatId: id,
  }
}


let nextMessageId = 1
export const ADD_MESSAGE = 'ADD_MESSAGE'
export const addMessage = (chatId, author, message) => {
  return {
    type: ADD_MESSAGE,
    chatId: chatId,
    messageId: nextMessageId++,
    createdAt: new Date(),
    author,
    message,
  }
}

export const UPDATE_PROFILE_STATUS = 'UPDATE_PROFILE_STATUS'
export const PROFILE_STATUS_WRITING = 'writing'
export const updateProfileStatus = (chatId, profile, status) => {
  return {
    type: UPDATE_PROFILE_STATUS,
    chatId,
    profile,
    status,
  }
}


export const rebootProfileStatusWhenPosting = store => next => action => {
  let result = next(action)

  if (action.type === ADD_MESSAGE) {
    store.dispatch(updateProfileStatus(action.chatId, action.author, null))
  }

  return result
}
