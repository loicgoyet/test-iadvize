import { createStore, applyMiddleware } from 'redux'
import rootReducer from './reducers'
import { composeWithDevTools } from 'redux-devtools-extension'
import { rebootProfileStatusWhenPosting } from './actions/chats'


const store = createStore(
  rootReducer,
  composeWithDevTools(
    applyMiddleware(rebootProfileStatusWhenPosting)
  )
)

export default store
