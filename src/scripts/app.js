import React from 'react'
import Chat from './components/containers/chat'

const App = () => (
  <main style={{padding: '2rem'}}>
    <h1>iAdvize test - Loïc Goyet</h1>

    <div className="chat-grid">
      <Chat id={1} profile="Alshon"/>
      <Chat id={1} profile="Carson"/>
    </div>
  </main>
)

export default App
