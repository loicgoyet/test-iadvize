import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import * as chatActions from '../../actions/chats'

import TalkLog from '../presentational/talkLog'
import Card from '../presentational/card'
import AddReplyForm from './addReplyForm'


class Chat extends Component {
  componentWillMount() {
    if (!this.props.createChat) return
    return this.props.addChat();
  }

  render() {
    const {profile, log, id, writers} = this.props

    return (
      <div>
        <h2>{this.props.profile}</h2>

        <Card modifiers={['chat']}>
          <TalkLog profile={profile} log={log} writers={writers}/>
          <AddReplyForm profile={profile} id={id}/>
        </Card>
      </div>
    )
  }
}

Chat.propTypes = {
  createChat: PropTypes.bool,
  profile: PropTypes.string.isRequired,
  id: PropTypes.number,
  addChat: PropTypes.func,
  writers: PropTypes.arrayOf(PropTypes.string),
  log: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    author: PropTypes.string,
    createdAt: PropTypes.object,
    message: PropTypes.string
  })).isRequired
}

const mapStateToProps = (state, ownProps) => {
  const chat = state.chats.find(chat => chat.id === ownProps.id)
  const log = chat ? chat.log : []
  const createChat = !chat
  const writers = !chat ? [] : Object.keys(chat.status).reduce((writers, profile) => {
    const profileStatus = chat.status[profile]

    if (profileStatus === chatActions.PROFILE_STATUS_WRITING && profile !== ownProps.profile) {
      return [
        ...writers,
        profile
      ]
    }

    return writers
  }, [])

  return {
    log,
    createChat,
    writers,
    profile: ownProps.profile,
    id: ownProps.id,

  }
}

const mapDispatchToProps = (dispatch, ownProps) => ({
  addChat: () => dispatch(chatActions.addChat(ownProps.id)),
})

// eslint-disable-next-line no-class-assign
Chat = connect(mapStateToProps, mapDispatchToProps)(Chat)

export default Chat
