import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import * as chatActions from '../../actions/chats'

import Button from '../presentational/button'
import Input from '../presentational/input'


const mapStateToProps = (state, ownProps) => {
  return {
    profile: ownProps.profile,
  }
}

const mapDispatchToProps = (dispatch, ownProps) => ({
  addReply: message => dispatch(chatActions.addMessage(ownProps.id, ownProps.profile, message)),
  updateStatus: (event) => {
    const message = event.target.value
    const isWriting = message.length
    const status = isWriting ? chatActions.PROFILE_STATUS_WRITING : null

    return dispatch(chatActions.updateProfileStatus(ownProps.id, ownProps.profile, status))
  }
})

let AddReplyForm = ({addReply, updateStatus}) => {
  let input

  return (
    <form className="add-message" onSubmit={e => {
      e.preventDefault()
      if (!input.value.trim()) return;
      addReply(input.value)
      input.value = ''
    }}>
      <Input
        reference={node => { input = node }}
        placeholder="type here"
        onChange={updateStatus}
      />
      <Button type="submit">OK</Button>
    </form>
  )
}

AddReplyForm.propTypes = {
  addReply: PropTypes.func,
  updateStatus: PropTypes.func,
}

AddReplyForm = connect(mapStateToProps, mapDispatchToProps)(AddReplyForm)

export default AddReplyForm
