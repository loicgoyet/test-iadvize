import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';

import Reply from './index';

test('renders without crashing', () => {
  renderer.create(
    <Reply>hello world !</Reply>
  );
});


test('display content', () => {
  const comp = mount(
    <Reply>lorem ipsum</Reply>
  );

  expect(comp.text()).toEqual('lorem ipsum');
})

test('display content and date', () => {
  const comp = mount(
    <Reply date={new Date()}>lorem ipsum</Reply>
  );

  expect(comp.find('.card').text()).toEqual('lorem ipsum');
  expect(comp.find('.date--from-now').text()).toEqual('a few seconds ago');
})

test('good orientation', () => {
  const compLeft = mount(
    <Reply>orientation left</Reply>
  );

  expect(compLeft.hasClass('reply--left')).toEqual(true);

  const compRight = mount(
    <Reply orientation="right">orientation left</Reply>
  );

  expect(compRight.hasClass('reply--right')).toEqual(true);
})
