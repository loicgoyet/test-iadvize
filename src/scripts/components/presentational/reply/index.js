import React from 'react'
import PropTypes from 'prop-types'

import Card from '../card'
import DateFromNow from '../dateFromNow'

const Reply = ({orientation = 'left', author, date, children}) => (
  <div className={`reply reply--${orientation}`}>
    {date && (
      <p className="reply__meta">
        <strong className="reply__author">{author}</strong> <DateFromNow date={date}/>
      </p>
    )}

    <Card
      theme={orientation === 'right' ? 'primary' : 'secondary'}
    >
      {children}
    </Card>
  </div>
)

Reply.propTypes = {
  children: PropTypes.node,
  orientation: PropTypes.oneOf(['left', 'right']),
  date: PropTypes.object,
  author: PropTypes.string,
}

export default Reply
