import React from 'react'
import PropTypes from 'prop-types'

const Card = ({theme = 'default', className = '',  modifiers = [], style = null, children}) => (
  <div
    className={`card card--${theme} ${className}${modifiers.map(modifier => ` card--${modifier}`)}`}
    style={style}
  >
    {children}
  </div>
)

Card.propTypes = {
  theme: PropTypes.oneOf(['default', 'primary', 'secondary']),
  className: PropTypes.string,
  children: PropTypes.node,
  modifiers: PropTypes.array,
  style: PropTypes.object,
}

export default Card
