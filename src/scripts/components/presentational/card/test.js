import React from 'react';
import renderer from 'react-test-renderer';
import {shallow} from 'enzyme';

import Card from './index';

test('renders without crashing', () => {
  renderer.create(
    <Card>hello world !</Card>
  );
});

test('sets primary theme by default', () => {
  const card = shallow(
    <Card>hello world !</Card>
  );

  expect(card.hasClass('card--default')).toEqual(true);
});

test('sets theme theme by props', () => {
  const card = shallow(
    <Card theme="primary">hello world !</Card>
  );

  expect(card.hasClass('card--primary')).toEqual(true);
});

test('set additionnal className', () => {
  const card = shallow(
    <Card className="pod">hello world !</Card>
  );

  expect(card.hasClass('pod')).toEqual(true);
});

test('set additionnal style', () => {
  const card = shallow(
    <Card style={{float: 'left'}}>hello world !</Card>
  );

  expect(card.html().match(/style="([^"]*)"/i)[1]).toBe('float:left;');
});

test('renders children', () => {
  const card = shallow(
    <Card>Hello world !</Card>
  );

  expect(card.text()).toEqual('Hello world !');
});
