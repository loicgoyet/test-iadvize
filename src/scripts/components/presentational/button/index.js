import React from 'react'
import PropTypes from 'prop-types'

const Button = ({type = "button", children}) => (
  <button
    className="btn"
    type={type}
  >
    {children}
  </button>
)

Button.propTypes = {
  type: PropTypes.oneOf(['button', 'submit', 'reset']),
  children: PropTypes.string,
}

export default Button
