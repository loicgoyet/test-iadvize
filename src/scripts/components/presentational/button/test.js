import React from 'react';
import {mount} from 'enzyme';

import Button from './index';

test('renders without crashing', () => {
  mount(
    <Button>hello world !</Button>
  );
});

test('set button as type by default', () => {
  const comp = mount(
    <Button>hello world !</Button>
  );

  expect(comp.html().match(/type="([^"]*)"/i)[1]).toBe('button');
});

test('set custom type', () => {
  const comp = mount(
    <Button type="submit">hello world !</Button>
  );

  expect(comp.html().match(/type="([^"]*)"/i)[1]).toBe('submit');
});

test('children does appear', () => {
  const comp = mount(
    <Button type="submit">hello world !</Button>
  );

  expect(comp.text()).toBe('hello world !');
});
