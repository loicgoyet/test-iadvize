import React, { Component } from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'

class DateFromNow extends Component {
  componentDidMount() {
    // will update the string displayed every minutes
    this.interval = setInterval(() => this.setState({ time: Date.now() }), 60000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const { date } = this.props

    return (
      <em className="date date--from-now">{moment(date).fromNow()}</em>
    )
  }
}

DateFromNow.propTypes = {
  date: PropTypes.object
}

export default DateFromNow
