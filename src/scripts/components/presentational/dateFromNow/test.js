import React from 'react';
import renderer from 'react-test-renderer';
import {shallow} from 'enzyme';

import DateFromNow from './index';

test('renders without crashing', () => {
  renderer.create(
    <DateFromNow date={new Date()}/>
  );
});

test('display the good text', () => {
  const comp = shallow(
    <DateFromNow date={new Date()}/>
  );

  expect(comp.text()).toEqual('a few seconds ago');
});
