import React from 'react'
import PropTypes from 'prop-types'

const Input = ({type = "text", placeholder, onChange, reference}) => (
  <input
    className="input"
    type={type}
    placeholder={placeholder}
    onChange={onChange}
    ref={reference}
  />
)

Input.propTypes = {
  type: PropTypes.string,
  placeholder: PropTypes.string,
  reference: PropTypes.func,
  onChange: PropTypes.func,
}

export default Input
