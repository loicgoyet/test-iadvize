import React from 'react';
import { mount } from 'enzyme';

import Input from './index'

test('renders without crashing', () => {
  mount(
    <Input/>
  );
});

test('renders placeholder', () => {
  const comp = mount(
    <Input placeholder="hello world !"/>
  );

  expect(comp.html().includes('placeholder="hello world !"')).toBe(true)
});
