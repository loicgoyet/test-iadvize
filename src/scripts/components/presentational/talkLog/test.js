import React from 'react';
import { mount } from 'enzyme';

import TalkLog from './index'



const props = {
  profile: 'Alshon',
  log: [
    {
      id: 1,
      author: 'Carson',
      createdAt: new Date(),
      message: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae dolorem similique numquam beatae, quo itaque. Libero quae, ad omnis odio!'
    },
    {
      id: 2,
      author: 'Alshon',
      createdAt: new Date(),
      message: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut veritatis in numquam doloribus perspiciatis voluptas placeat corporis reiciendis ratione rerum illo temporibus sequi dolores odio, earum aliquid qui officia maiores. Veritatis quis sequi quae odit fugit. Corrupti deserunt quos ut aut soluta ad impedit, consequatur veritatis maiores placeat vitae, magnam fugiat cumque dignissimos autem. Porro praesentium culpa sequi at id!'
    },
    {
      id: 3,
      author: 'Carson',
      createdAt: new Date(),
      message: 'Lorem ipsum dolor sit amet.'
    },
    {
      id: 4,
      author: 'Carson',
      createdAt: new Date(),
      message: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia quam id, commodi numquam earum corporis itaque dignissimos minima possimus illo, rem nihil iure non dolor magni, doloremque minus porro soluta.'
    },
    {
      id: 5,
      author: 'Carson',
      createdAt: new Date(),
      message: 'Lorem ipsum dolor sit amet, consectetur adipisicing.'
    },
    {
      id: 6,
      author: 'Alshon',
      createdAt: new Date(),
      message: 'ok'
    },
  ],
  test: true,
}


test('renders without crashing', () => {
  mount(
    <TalkLog {...props}/>
  );
});

test('display well messages', () => {
  const comp = mount(
    <TalkLog {...props}/>
  );

  expect(comp.find('.card').first().text()).toEqual('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae dolorem similique numquam beatae, quo itaque. Libero quae, ad omnis odio!');
  expect(comp.find('.card').last().text()).toEqual('ok');
});
