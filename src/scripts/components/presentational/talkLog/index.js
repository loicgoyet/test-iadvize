import React, { Component } from 'react'
import PropTypes from 'prop-types'

import Reply from '../reply'

class TalkLog extends Component {
  componentDidMount() {
    this.scrollToBottom()
  }

  componentDidUpdate() {
    this.scrollToBottom()
  }

  scrollToBottom() {
    // @NOTE A known bug exists with enzyme on the scrollIntoView function on
    // dom elements. As the scroll to the bottom TalkLog is not a pure visual
    // feature and does not count into the unit test scope, we avoid the
    // scrollToBottom method to run when the `test` prop is setted as true
    if (this.props.test) return;

    const lastReply = this.wrapper.querySelector('.reply:last-child')
    if (lastReply) return lastReply.scrollIntoView({ behavior: "smooth" })
  }

  render() {
    const { log, profile, writers = [] } = this.props

    return (
      <div className="talk-log" ref={(el) => { this.wrapper = el }}>
        {log.map(reply => (
          <Reply
            key={reply.id}
            orientation={reply.author === profile ? 'right' : 'left'}
            author={reply.author !== profile ? reply.author : null}
            date={reply.createdAt}
          >
            {reply.message}
          </Reply>
        ))}

        {writers.length > 0 && (
          <Reply
            orientation={'left'}
          >
            {writers.join(', ')} typing...
          </Reply>
        )}
      </div>
    )
  }
}

TalkLog.defaultProps = {
  test: false,
};

TalkLog.propTypes = {
  profile: PropTypes.string.isRequired,
  test: PropTypes.bool,
  writers: PropTypes.arrayOf(PropTypes.string),
  log: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    author: PropTypes.string,
    createdAt: PropTypes.object,
    message: PropTypes.string
  })).isRequired
}

export default TalkLog
