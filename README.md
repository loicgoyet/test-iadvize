# iAdvize test from Loïc Goyet

## Build the code
If you want to build just once the code, you can run the following command :

```sh
yarn build
```

This will create a `/dist` dir with a `index.html` file that you can run into your browser


## Run the code with a localserver
If you'd rather run the code into a localserver managed by gulp and with some livereloading just prompt :

```sh
yarn serve
```

The project will be automatically working into your browser.


## Read the code
All the working files are located into the `/src` folder.

* `/src/scripts` dir contains all the react app with a state managed a redux store written in es6.
* `/src/style` contains all the scss stylesheet used to generate the style code.

Any question on the code ? you can mail me at `loic.goyet@gmail.com`
